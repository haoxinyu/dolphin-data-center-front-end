import TabsView from '@/layouts/tabs/TabsView'
import BlankView from '@/layouts/BlankView'
import PageView from '@/layouts/PageView'

// 路由配置
const options = {
  routes: [
    {
      path: '/login',
      name: '登录页',
      component: () => import('@/pages/login'),
    },
    {
      path: '*',
      name: '404',
      component: () => import('@/pages/exception/404'),
    },
    {
      path: '/403',
      name: '403',
      component: () => import('@/pages/exception/403'),
    },
    {
      path: '/',
      name: '首页',
      component: TabsView,
      redirect: '/login',
      children: [
        {
          path: 'workplace',
          name: '工作台',
          meta: {
            page: {
              closable: false,
            },
          },
          component: () => import('@/pages/workplace'),
        },
        {
          path: 'dashboard',
          name: 'Dashboard',
          meta: {
            icon: 'dashboard',
          },
          component: BlankView,
          children: [
            {
              path: 'analysis',
              name: '系统统计',
              component: () => import('@/pages/dashboard/analysis'),
            },
          ],
        },
        {
          path: 'authorityManagement',
          name: '权限管理',
          meta: {
            icon: 'profile',
          },
          component: BlankView,
          children: [
            {
              path: 'roleManagement',
              name: '角色管理',
              component: () =>
                import('@/pages/authorityManagement/roleManagement'),
            },
            {
              path: 'characterDetails',
              name: '角色详情',
              component: () =>
                import('@/pages/authorityManagement/characterDetails'),
              meta: {
                invisible: true,
              },
            },
            {
              path: 'staffManagement',
              name: '员工管理',
              component: () =>
                import('@/pages/authorityManagement/staffManagement'),
            },
            {
              path: 'personalDetails',
              name: '员工详情',
              component: () =>
                import('@/pages/authorityManagement/personalDetails'),
              meta: {
                invisible: true,
              },
            },
          ],
        },
        {
          path: 'systemConfig',
          name: '系统配置',
          meta: {
            icon: 'check-circle-o',
          },
          component: PageView,
          children: [
            {
              path: 'contractingSubjectManagement',
              name: '签约主体管理',
              component: () =>
                import('@/pages/systemConfig/contractingSubjectManagement'),
            },
          ],
        },
        {
          path: 'quickEntry',
          name: '快接入口',
          meta: {
            icon: 'check-circle-o',
          },
          component: PageView,
          children: [
            {
              path: 'employeeBatchProcessing',
              name: '员工批量处理',
              icon: 'check-circle-o',
              component: () =>
                import('@/pages/quickEntry/employeeBatchProcessing'),
            },
            {
              path: 'employeeManagement',
              name: '雇员管理',
              icon: 'check-circle-o',
              component: () => import('@/pages/quickEntry/employeeManagement'),
            },
            {
              path: 'uploadExcel',
              name: '上传文件',
              icon: 'check-circle-o',
              component: () => import('@/pages/components/uploadExcel'),
            },
            {
              path: 'employeeDetails',
              name: '雇员详情',
              icon: 'check-circle-o',
              component: () => import('@/pages/quickEntry/employeeDetails'),
              // meta: {
              //   invisible: true,
              // },
            },
            {
              path: 'contractManagement',
              name: '合同管理',
              component: () => import('@/pages/quickEntry/contractManagement'),
            },
            {
              path: 'batchProcessing',
              name: '批量处理',
              icon: 'check-circle-o',
              component: () =>
                import('@/pages/components/batchProcessing/index'),
            },
          ],
        },
        // {
        //   name: '验权表单',
        //   path: 'auth/form',
        //   meta: {
        //     icon: 'file-excel',
        //     authority: {
        //       permission: 'form',
        //     },
        //   },
        //   component: () => import('@/pages/form/basic'),
        // },
        // {
        //   name: 'Ant Design Vue',
        //   path: 'list',
        //   meta: {
        //     icon: 'ant-design',
        //     // link: 'https://www.antdv.com/docs/vue/introduce-cn/'
        //   },
        //   component: () => import('@/pages/list/QueryList'),
        // },
        // {
        //   name: '使用文档',
        //   path: 'document',
        //   meta: {
        //     icon: 'file-word',
        //     link: 'https://iczer.gitee.io/vue-antd-admin-docs/'
        //   }
        // }
      ],
    },
  ],
}

export default options
