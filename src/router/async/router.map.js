// 视图组件
const view = {
  tabs: () => import('@/layouts/tabs'),
  blank: () => import('@/layouts/BlankView'),
  page: () => import('@/layouts/PageView'),
}

// 路由组件注册
const routerMap = {
  login: {
    authority: '*',
    path: '/login',
    component: () => import('@/pages/login'),
  },
  root: {
    path: '/',
    name: '首页',
    redirect: '/login',
    component: view.tabs,
  },
  dashboard: {
    name: 'Dashboard',
    component: view.blank,
  },
  workplace: {
    name: '工作台',
    path: 'workplace',
    component: () => import('@/pages/workplace'),
  },

  analysis: {
    name: '分析页',
    component: () => import('@/pages/dashboard/analysis'),
  },
  authorityManagement: {
    path: 'authorityManagement',
    name: '权限管理',
    meta: {
      icon: 'profile',
    },
    component: view.blank,
  },
  roleManagement: {
    name: '角色管理',
    path: 'roleManagement',
    component: () => import('@/pages/authorityManagement/roleManagement'),
  },
  characterDetails: {
    path: 'characterDetails',
    name: '角色详情',
    component: () => import('@/pages/authorityManagement/characterDetails'),
    meta: {
      invisible: true,
    },
  },
  staffManagement: {
    path: 'staffManagement',
    name: '员工管理',
    component: () => import('@/pages/authorityManagement/staffManagement'),
  },
  personalDetails: {
    path: 'personalDetails',
    name: '员工详情',
    component: () => import('@/pages/authorityManagement/personalDetails'),
    meta: {
      invisible: true,
    },
  },
  systemConfig: {
    path: 'systemConfig',
    name: '系统配置',
    meta: {
      icon: 'check-circle-o',
    },
    component: view.blank,
  },
  contractingSubjectManagement: {
    path: 'contractingSubjectManagement',
    name: '签约主体管理',
    component: () =>
      import('@/pages/systemConfig/contractingSubjectManagement'),
  },
  quickEntry: {
    path: 'quickEntry',
    name: '快接入口',
    meta: {
      icon: 'check-circle-o',
    },
    component: view.page,
  },
  employeeBatchProcessing: {
    path: 'employeeBatchProcessing',
    name: '员工批量处理',
    icon: 'check-circle-o',
    component: () => import('@/pages/quickEntry/employeeBatchProcessing'),
  },
  employeeManagement: {
    path: 'employeeManagement',
    name: '雇员管理',
    icon: 'check-circle-o',
    component: () => import('@/pages/quickEntry/employeeManagement'),
  },
  uploadExcel: {
    path: 'uploadExcel',
    name: '上传文件',
    icon: 'check-circle-o',
    component: () => import('@/pages/components/uploadExcel'),
  },
  employeeDetails: {
    path: 'employeeDetails',
    name: '雇员详情',
    icon: 'check-circle-o',
    component: () => import('@/pages/quickEntry/employeeDetails'),
    meta: {
      invisible: true,
    },
  },
  contractManagement: {
    path: 'contractManagement',
    name: '合同管理',
    component: () => import('@/pages/quickEntry/contractManagement'),
  },
  batchProcessing: {
    path: 'batchProcessing',
    name: '批量处理',
    icon: 'check-circle-o',
    component: () => import('@/pages/components/batchProcessing/index'),
  },
  exp403: {
    authority: '*',
    name: 'exp403',
    path: '403',
    component: () => import('@/pages/exception/403'),
  },
  exp404: {
    name: 'exp404',
    path: '404',
    component: () => import('@/pages/exception/404'),
  },
  exp500: {
    name: 'exp500',
    path: '500',
    component: () => import('@/pages/exception/500'),
  },
}
export default routerMap
