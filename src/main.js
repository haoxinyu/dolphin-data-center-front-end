/*
 * @Author: your name
 * @Date: 2021-04-08 10:43:26
 * @LastEditTime: 2021-04-26 13:45:04
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \dolphin-data-center-front-end\src\main.js
 */
import Vue from 'vue'
import App from './App.vue'
import { initRouter } from './router'
import './theme/index.less'
import Antd from 'ant-design-vue'
import Viser from 'viser-vue'
import '@/mock'
import store from './store'
import 'animate.css/source/animate.css'
import Plugins from '@/plugins'
import { initI18n } from '@/utils/i18n'
import bootstrap from '@/bootstrap'
import 'moment/locale/zh-cn'
import { storage } from './utils/util' // 本地存储

Vue.prototype.$Storage = storage

const router = initRouter(store.state.setting.asyncRoutes)
const i18n = initI18n('CN', 'US')

Vue.use(Antd)
Vue.config.productionTip = false
Vue.use(Viser)
Vue.use(Plugins)

bootstrap({ router, store, i18n, message: Vue.prototype.$message })
new Vue({
  router,
  store,
  i18n,
  render: (h) => h(App),
}).$mount('#app')
