/*
 * @Author: your name
 * @Date: 2021-04-21 15:15:29
 * @LastEditTime: 2021-04-22 16:52:22
 * @LastEditors: Please set LastEditors
 * @Description: 合同管理接口
 * @FilePath: \dolphin-data-center-front-end\src\services\contract.js
 */
import {
  ADD_TEMPLATE,
  TEMPLATE_DETAIL,
  TEMPLATE_URL,
  DELETE_TEMPLATE,
  TEMPLATE_LIST,
  UPLOAD_FILE,
  PROJECT_LIST,
} from '@/services/api'
import { request, METHOD } from '@/utils/request'
// 查询模板列表
export async function queryTemplate(params) {
  return request(TEMPLATE_LIST, METHOD.GET, params)
}
// 添加、修改模板
export async function addTemplate(params) {
  return request(ADD_TEMPLATE, METHOD.GET, params)
}
// 删除模板
export async function deleteTemplate(params) {
  return request(DELETE_TEMPLATE, METHOD.GET, params)
}
// 查看模板详情
export async function templateDetail(params) {
  return request(TEMPLATE_DETAIL, METHOD.GET, params)
}
// 查看模板
export async function template(params) {
  return request(TEMPLATE_URL, METHOD.GET, params)
}
// 上传模板
export async function uploadTemplate(params) {
  return request(UPLOAD_FILE, METHOD.POST, params)
}
// 查询项目列表
export async function queryProject() {
  return request(PROJECT_LIST, METHOD.GET)
}
export default {
  queryTemplate,
  addTemplate,
  deleteTemplate,
  templateDetail,
  template,
  uploadTemplate,
  queryProject,
}
