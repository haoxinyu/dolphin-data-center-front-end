import {
  uploadExcelApiPath,
  confirmUploadApiPath,
  getLatestImportHistoryRecordApiPath,
  batchEntryPositionApiPath,
  employeeManageListApiPath,
  employeeDetailgetByIdApiPath,
  attachmentGetUserIdApiPath,
  entryByPitchApiPath,
  batchCancelEntryApiPath,
  projectGetListApiPath,
  downloadTempApiPath,
  employeeDimissionApiPath,
} from './api'

import Qs from 'qs'

import { METHOD, request } from '@/utils/request'

export function batchEntryPositionApi(params) {
  return request(batchEntryPositionApiPath, METHOD.POST, params)
}

export function uploadExcelApi(params, config) {
  return request(`${uploadExcelApiPath}`, METHOD.POST, params, config)
}

export function confirmUploadApi(params) {
  return request(confirmUploadApiPath, METHOD.POST, params)
}

export function getLatestImportHistoryRecordApi(params) {
  return request(getLatestImportHistoryRecordApiPath, METHOD.GET, params)
}

export function employeeManageListApi(params) {
  return request(employeeManageListApiPath, METHOD.POST, params)
}

export function employeeDetailgetByIdApi(params) {
  return request(employeeDetailgetByIdApiPath, METHOD.GET, params)
}

export function attachmentGetUserIdApi(params) {
  return request(attachmentGetUserIdApiPath, METHOD.GET, params)
}

export function entryByPitchApi(params) {
  return request(entryByPitchApiPath, METHOD.POST, Qs.stringify(params))
}

export function batchCancelEntryApi(params) {
  return request(batchCancelEntryApiPath, METHOD.POST, Qs.stringify(params))
}

export function projectGetListApi(params) {
  return request(projectGetListApiPath, METHOD.GET, params)
}

export function downloadTempApi(params) {
  return request(downloadTempApiPath, METHOD.GET, params)
}

export function employeeDimissionApi(params) {
  return request(employeeDimissionApiPath, METHOD.POST, params)
}

export default {
  batchEntryPositionApi,
  uploadExcelApi,
  confirmUploadApi,
  employeeManageListApi,
  getLatestImportHistoryRecordApi,
  employeeDetailgetByIdApi,
  attachmentGetUserIdApi,
  entryByPitchApi,
  batchCancelEntryApi,
  projectGetListApi,
  downloadTempApi,
}
