/*
 * @Author: your name
 * @Date: 2021-04-26 14:58:28
 * @LastEditTime: 2021-04-26 16:40:12
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \dolphin-data-center-front-end\src\services\employeeDetail.js
 */
import {
  PERSONAL_INFO,
  EDU_HISTORY,
  JOB_HISTORY,
  FAMILY_MEMBER,
  ATTACHMENT_LIST,
  attachmentAddOrUpdateflowersApiPath,
} from './api'

import { METHOD, request } from '@/utils/request'

export function personalInfo(params) {
  return request(PERSONAL_INFO, METHOD.GET, params)
}

export function eduHistory(params) {
  return request(EDU_HISTORY, METHOD.GET, params)
}

export function jobHistory(params) {
  return request(JOB_HISTORY, METHOD.GET, params)
}

export function familyMember(params) {
  return request(FAMILY_MEMBER, METHOD.GET, params)
}

export function attachmentList(params) {
  return request(ATTACHMENT_LIST, METHOD.GET, params)
}

export function attachmentAddOrUpdateflowersApi(params) {
  return request(attachmentAddOrUpdateflowersApiPath, METHOD.POST, params)
}

export default {
  personalInfo,
  eduHistory,
  jobHistory,
  familyMember,
  attachmentList,
}
