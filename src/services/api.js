/*
 * @Author: your name
 * @Date: 2021-04-27 14:22:09
 * @LastEditTime: 2021-04-27 14:54:41
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \dolphin-data-center-front-end\src\services\api.js
 */
//跨域代理前缀
// const API_PROXY_PREFIX = '/api'
//base url
// const BASE_URL = process.env.NODE_ENV === 'production' ? process.env.VUE_APP_API_BASE_URL : API_PROXY_PREFIX
const BASE_URL = process.env.VUE_APP_API_BASE_URL
console.log('BASE_URL', BASE_URL)

module.exports = {
  TK_LOGIN: `${BASE_URL}/catfish/client/login`,
  get_OSS_config: `${BASE_URL}/catfish/oss/getConfig`,
  LOGIN: `${BASE_URL}/login`,
  ROUTES: `${BASE_URL}/routes`,
  GOODS: `${BASE_URL}/goods`,
  GOODS_COLUMNS: `${BASE_URL}/columns`,
  GETSignature: `${BASE_URL}/api/getSignature`,
  downloadTemp: `${BASE_URL}/catfish/uploadData/downloadTemp`,
  /**
   * @description: 角色管理
   * @param {*}
   * @return {*}
   */
  ROLE_LIST: `${BASE_URL}/catfish/role/getRoleList`,
  UPDATE_ROLE: `${BASE_URL}/catfish/role/saveRole`,
  DELETE_ROLE: `${BASE_URL}/catfish/role/logicDelete`,
  PERMISSION_TREE: `${BASE_URL}/catfish/role/getAuthorityList`,
  QUERY_PERMISSION: `${BASE_URL}/catfish/role/getRoleAuthorityList`,
  ROLE_USER: `${BASE_URL}/catfish/role/getUserListByRole`,
  REMOVE_USER: `${BASE_URL}/catfish/role/bindUserRole`,
  ROLENAME_LIST: `${BASE_URL}/catfish/role/getRolelList`,
  /**
   * @description: 合同模板
   * @param {*}
   * @return {*}
   *
   */
  ADD_TEMPLATE: `${BASE_URL}/channel/eqbTemplateBase/addAndUpdateTemplateBase`,
  TEMPLATE_DETAIL: `${BASE_URL}/channel/eqbTemplateBase/getTemplateDetails`,
  TEMPLATE_URL: `${BASE_URL}/channel/eqbTemplateBase/getTemplateUrlById`,
  DELETE_TEMPLATE: `${BASE_URL}/channel/eqbTemplateBase/deleteById`,
  TEMPLATE_LIST: `${BASE_URL}/channel/eqbTemplateBase/getPageByProjectBaseName`,
  UPLOAD_FILE: `https://rlzyy.youlanw.com/channel/eqbTemplateBase/eqbUploadByUrl`,
  PROJECT_LIST: `${BASE_URL}/channel/eqbTemplateBase/getProjectList`,
  /**
   * @description: 员工管理
   * @param {*}
   * @return {*}
   */
  STAFF_INFO: `${BASE_URL}/catfish/auth/user/getUserAuthInfo`,
  STAFF_LIST: `${BASE_URL}/catfish/auth/user/getPage`,
  TOP_DEPARTMENT: `${BASE_URL}/catfish/dept/getTopDeptList`,
  SUB_DEPARTMENT: `${BASE_URL}/catfish/dept/getDeptStructure`,
  /**
   * @description: 雇员管理
   * @param {*}
   * @return {*}
   */
  uploadExcelApiPath: `${BASE_URL}/catfish/uploadData/upload`, // 上传Excel文件
  confirmUploadApiPath: `${BASE_URL}/catfish/uploadData/confirmUpload`, // 二次匹配表头后，上传文件数据
  downloadTempApiPath: `${BASE_URL}/catfish/uploadData/downloadTemp`, // 根据类型下载批量导入模板
  getLatestImportHistoryRecordApiPath: `${BASE_URL}/catfish/upload/recor/getList`, // 获取当前用户最近30天导入历史
  employeeManageListApiPath: `${BASE_URL}/flowers/employee/pageList`, // 列表查询
  employeeDetailgetByIdApiPath: `${BASE_URL}/flowers/employee/getById`, // 列表详情
  attachmentGetUserIdApiPath: `${BASE_URL}/flowers/attachment/getByUserId`, // 附件详情展示
  entryByPitchApiPath: `${BASE_URL}/flowers/employee/entry`, // 入职（可批量）
  batchCancelEntryApiPath: `${BASE_URL}/flowers/employee/batchCancelEntry`, // 取消入职(可批量)
  projectGetListApiPath: `${BASE_URL}/flowers/outside/project/getList`, // 外服项目列表
  employeeDimissionApiPath: `${BASE_URL}/flowers/employee/dimission`, //  雇员离职

  batchEntryPositionApiPath: `${BASE_URL}/catfish/EmployeeManager/batchEntry`, // 批量入职
  /**
   * @description:雇员详情
   * @param {*}
   * @return {*}
   */
  PERSONAL_INFO: `${BASE_URL}/flowers/user/getById`, // 雇员个人信息
  EDU_HISTORY: `${BASE_URL}/flowers/eduHistory/getByUserId`, // 教育经历
  JOB_HISTORY: `${BASE_URL}/flowers/jobHistory/getByUserId`, // 工作经历
  FAMILY_MEMBER: `${BASE_URL}/flowers/contact/getByUserId`, // 家庭成员
  ATTACHMENT_LIST: `${BASE_URL}/flowers/attachment/getByUserId`, // 附件
  attachmentAddOrUpdateflowersApiPath: `${BASE_URL}/flowers/attachment/addOrUpdate`, // 附件信息-新增/修改
}
