/*
 * @Author: your name
 * @Date: 2021-04-21 15:43:49
 * @LastEditTime: 2021-04-22 16:57:57
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \dolphin-data-center-front-end\src\services\emplyee.js
 */
import {
  STAFF_INFO,
  STAFF_LIST,
  TOP_DEPARTMENT,
  SUB_DEPARTMENT,
} from '@/services/api'
import { request, METHOD } from '@/utils/request'
// 获取员工列表
export async function queryEmplyee(params) {
  return request(STAFF_LIST, METHOD.POST, params)
}
// 获取员工信息
export async function employeeInfo(params) {
  return request(STAFF_INFO, METHOD.GET, params)
}
// 获取组织架构一级列表
export async function topDepartment() {
  return request(TOP_DEPARTMENT, METHOD.GET)
}
// 获取组织架构下级列表
export async function subDepartment(params) {
  return request(SUB_DEPARTMENT, METHOD.GET, params)
}

export default {
  queryEmplyee,
  employeeInfo,
  topDepartment,
  subDepartment,
}
