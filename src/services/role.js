/*
 * @Author: your name
 * @Date: 2021-04-20 10:30:53
 * @LastEditTime: 2021-04-25 17:20:51
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \dolphin-data-center-front-end\src\services\permission.js
 */
import {
  ROLE_LIST,
  UPDATE_ROLE,
  DELETE_ROLE,
  PERMISSION_TREE,
  QUERY_PERMISSION,
  ROLE_USER,
  REMOVE_USER,
  ROLENAME_LIST,
} from '@/services/api'
import { request, METHOD } from '@/utils/request'
// 获取角色列表   完成
export async function roleList(params) {
  return request(ROLE_LIST, METHOD.POST, params)
}
// 新增 修改角色  完成
export async function updateRole(params) {
  return request(UPDATE_ROLE, METHOD.POST, params)
}
// 删除角色  完成
export async function deleteRole(params) {
  return request(DELETE_ROLE, METHOD.GET, params)
}
// 人员树
export async function permissionTree() {
  return request(PERMISSION_TREE, METHOD.POST)
}
// 查询角色权限
export async function rolePermission(params) {
  return request(QUERY_PERMISSION, METHOD.GET, params)
}
// 角色下的用户
export async function roleUser(params) {
  return request(ROLE_USER, METHOD.POST, params)
}
// 移除用户
export async function removeUser(params) {
  return request(REMOVE_USER, METHOD.POST, params)
}
// 添加成员
export async function addMemeber(params) {
  return request(REMOVE_USER, METHOD.POST, params)
}
//角色下拉列表
export async function roleNameList() {
  return request(ROLENAME_LIST, METHOD.GET)
}
export default {
  roleList,
  updateRole,
  deleteRole,
  permissionTree,
  rolePermission,
  roleUser,
  removeUser,
  addMemeber,
}
