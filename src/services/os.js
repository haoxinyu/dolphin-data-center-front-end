/*
 * @Author: your name
 * @Date: 2021-04-25 15:09:20
 * @LastEditTime: 2021-04-25 15:10:14
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \dolphin-data-center-front-end\src\services\os.js
 */
import { get_OSS_config } from './api'
import { METHOD, request } from '@/utils/request'

export async function getOSSConfig(params) {
  return request(get_OSS_config, METHOD.GET, params)
}

export default { getOSSConfig }
