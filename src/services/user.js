/*
 * @Author: your name
 * @Date: 2021-04-16 10:51:46
 * @LastEditTime: 2021-04-20 09:41:31
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \dolphin-data-center-front-end\src\services\user.js
 */
import { LOGIN, ROUTES, GETSignature, TK_LOGIN } from '@/services/api'
import { request, METHOD, removeAuthorization } from '@/utils/request'

/**
 * 登录服务
 * @param name 账户名
 * @param password 账户密码
 * @returns {Promise<AxiosResponse<T>>}
 */
export async function login(name, password) {
  return request(LOGIN, METHOD.POST, {
    name,
    password,
  })
}

export async function getRoutesConfig() {
  return request(ROUTES, METHOD.GET)
}
export function getSignature() {
  console.log('GETSignature', GETSignature)
  return request(GETSignature, METHOD.GET)
}
// 钉钉扫码登录
export async function tkLogin(DATA) {
  return request(TK_LOGIN, METHOD.POST, DATA)
}
/**
 * 退出登录
 */
export function logout() {
  localStorage.removeItem(process.env.VUE_APP_ROUTES_KEY)
  localStorage.removeItem(process.env.VUE_APP_PERMISSIONS_KEY)
  localStorage.removeItem(process.env.VUE_APP_ROLES_KEY)
  removeAuthorization()
}
export default {
  login,
  logout,
  getRoutesConfig,
  getSignature,
  tkLogin,
}
