/*
 * @Author: your name
 * @Date: 2021-04-07 15:47:47
 * @LastEditTime: 2021-04-10 10:14:29
 * @LastEditors: your name
 * @Description: In User Settings Edit
 * @FilePath: \dolphin-data-center-front-end\src\pages\workplace\i18n.js
 */
module.exports = {
  messages: {
    CN: {
      project: '项目数',
      ranking: '团队排名',
      visit: '项目访问',
      progress: '进行中的项目',
      all: '全部项目',
      access: '快捷导航',
      dynamic: '动态',
      degree: '指数',
      team: '团队',
      add: '添加'
    },
    HK: {
      project: '項目數',
      ranking: '團隊排名',
      visit: '項目訪問',
      progress: '進行中的項目',
      all: '全部項目',
      access: '快速開始/便捷導航',
      dynamic: '動態',
      degree: '指數',
      team: '團隊',
      add: '添加'
    },
    US: {
      project: 'Project',
      ranking: 'Ranking',
      visit: 'Visit',
      progress: 'Projects in progress',
      all: 'All projects',
      access: 'Quick start / Easy navigation',
      dynamic: 'Dynamic',
      degree: 'degree',
      team: 'Team',
      add: 'Add'
    },
  }
}
