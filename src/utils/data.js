let datas = {
  tableData: [
    {
      id: 2,
      name: '张三',
      proName: '测试项目名称',
      IdNum: '43094419952026325',
      telNum: '18848960162',
      staffStatus: 1,
      createdDate: '2021-04-10 14:30',
      entryDate: '2020-04-15',
      updateDate: '2021-04-10 14:30',
    },
    {
      id: 5,
      name: '张四',
      proName: '测试项目名称',
      IdNum: '43094419952026325',
      telNum: '18848960162',
      staffStatus: 2,
      createdDate: '2021-04-10 14:30',
      entryDate: '2020-04-15',
      updateDate: '2021-04-10 14:30',
    },
  ],
  columns: [
    {
      title: '姓名',
      dataIndex: 'name',
      searchAble: true,
    },
    {
      title: '项目名称',
      dataIndex: 'project',
      searchAble: true,
      scopedSlots: { customRender: 'project' },
    },
    {
      title: '身份证号',
      dataIndex: 'idCard',
      searchAble: true,
    },
    {
      title: '手机号码',
      dataIndex: 'mobile',
      searchAble: true,
    },
    {
      title: '员工状态',
      dataIndex: 'status',
      key: 'status',
      scopedSlots: { customRender: 'status' },
      dataType: 'select',
      searchAble: true,
    },
    {
      title: '创建时间',
      dataType: 'createTime',
      dataIndex: 'createTime',
      searchAble: true,
      scopedSlots: { customRender: 'createTime' },
    },
    {
      title: '入职时间',
      dataType: 'entryTime',
      dataIndex: 'entryTime',
      searchAble: true,
      scopedSlots: { customRender: 'entryTime' },
    },
    {
      title: '更新时间',
      dataType: 'updateTime',
      dataIndex: 'updateTime',
      searchAble: false,
      scopedSlots: { customRender: 'updateTime' },
    },
    {
      title: '操作',
      key: 'setting',
      dataIndex: 'setting',
      scopedSlots: { customRender: 'setting' },
    },
  ],
  columns1: [
    {
      title: '类型',
      dataIndex: 'status',
      searchAble: true,
      scopedSlots: { customRender: 'status' },
    },
    {
      title: '姓名',
      dataIndex: 'name',
      searchAble: true,
    },
    {
      title: '项目名称',
      dataIndex: 'project',
      searchAble: true,
      scopedSlots: { customRender: 'project' },
    },
    {
      title: '身份证',
      dataIndex: 'idCard',
      searchAble: true,
    },
    {
      title: '手机号',
      dataIndex: 'mobile',
      searchAble: true,
    },
    {
      title: '提交日期',
      dataIndex: 'contract',
      searchAble: true,
      scopedSlots: { customRender: 'contract' },
    },
    {
      title: '操作',
      key: 'setting1',
      dataIndex: 'setting1',
      scopedSlots: { customRender: 'setting1' },
    },
  ],
  importHistoryColumn: [
    {
      title: '操作类型',
      dataIndex: 'batchTypeDesc',
    },
    {
      title: '操作时间',
      dataIndex: 'createTime',
      scopedSlots: { customRender: 'createTime' },
    },
    {
      title: '处理状态',
      dataIndex: 'statusDesc',
      scopedSlots: { customRender: 'statusDesc' },
    },
    {
      title: '结果查看',
      dataIndex: 'total',
      scopedSlots: { customRender: 'total' },
    },
    {
      title: '操作',
      dataIndex: 'opera',
      scopedSlots: { customRender: 'opera' },
    },
  ],
  importHistoryData: [
    {
      id: 1,
      batchTypeDesc: '批量新增',
      createTime: 1619328484187,
      statusDesc: '处理中',
      total: 36,
      sourceFileUrl: 'http://wwww.baidu.com',
      failFileUrl: 'http://wwww.baidu2.com',
    },
  ],
  eduExperienceColumns: [
    {
      title: '学校',
      width: '15%',
      dataIndex: 'schoolName',
      scopedSlots: { customRender: 'schoolName' },
    },
    {
      title: '专业',
      width: '15%',
      dataIndex: 'majorName',
      scopedSlots: { customRender: 'majorName' },
    },
    {
      title: '开始时间',
      width: '25%',
      dataIndex: 'beginDate',
      scopedSlots: { customRender: 'beginDate' },
    },
    {
      title: '结束时间',
      width: '25%',
      dataIndex: 'endDate',
      scopedSlots: { customRender: 'endDate' },
    },
    {
      title: '操作',
      width: '15%',
      dataIndex: 'opera',
      scopedSlots: { customRender: 'opera' },
    },
  ],
  eduExperienceDatas: [
    {
      key: '1',
      school: '复旦大学',
      major: '临床医学',
      beginDate: '2017-08-16',
      endDate: '2020-05-15',
    },
    {
      key: '2',
      school: '同济大学',
      major: '医学',
      beginDate: '2017-08-16',
      endDate: '2020-05-15',
    },
  ],
  jobExperienceColumns: [
    {
      title: '企业名称',
      dataIndex: 'companyName',
      scopedSlots: { customRender: 'companyName' },
    },
    {
      title: '职位',
      dataIndex: 'postName',
      scopedSlots: { customRender: 'postName' },
    },
    {
      title: '简述',
      dataIndex: 'content',
      scopedSlots: { customRender: 'content' },
    },
    {
      title: '入职时间',
      dataIndex: 'beginDate',
      scopedSlots: { customRender: 'beginDate' },
    },
    {
      title: '离职时间',
      dataIndex: 'endDate',
      scopedSlots: { customRender: 'endDate' },
    },
    {
      title: '操作',
      dataIndex: 'opera',
      scopedSlots: { customRender: 'opera' },
    },
  ],
  jobExperienceDatas: [
    {
      key: '1',
      corpName: '苏州天坤电子有限公司',
      position: '高级焊工',
      desc: '根据班组长下达的生产任务,合理安排生产,保质保量按时完成当天的工作',
      entryDate: '2012-05-16',
      leaveDate: '2021-05-15',
    },
    {
      key: '2',
      corpName: '苏州天坤电子有限公司',
      position: '高级焊工',
      desc: '根据班组长下达的生产任务,合理安排生产,保质保量按时完成当天的工作',
      entryDate: '2012-05-16',
      leaveDate: '2021-05-15',
    },
  ],
  homeMemberExperienceColumns: [
    {
      title: '姓名',
      dataIndex: 'name',
    },
    {
      title: '关系',
      dataIndex: 'relation',
    },
    {
      title: '电话',
      dataIndex: 'mobile',
    },
  ],
  homeMemberExperienceDatas: [
    {
      key: '1',
      name: '欧阳天涯',
      relationship: '父女',
      tel: '18848960169',
    },
  ],
  personInfoDatas: [
    {
      name: '欧阳娜娜',
      mobile: '18848960162',
      email: '18848960162@39.com',
      gender: 1,
      age: 26,
      marriage: 1,
      nation: '汉族',
      education: '本科',
      political: '群众',
      census: '上海市宝山区',
      address: '上海市宝山区长江南路',
      socialSecurityNo: 4154646464658786,
      accumulationFundNo: 4654446546575,
      schoolName: '华东师范',
      schoolMajor: '高级技工',
      firstJobDate: '2018',
      cardNum: 621700245002525832,
      bankAddress: '上海市牡丹江路支行',
      bankName: '中国建设银行',
      antuCardType: '身份证',
      idCard: 6217252554354545454,
      birthday: '1990-05-15',
      urgencyPersonName: '张三',
      urgencyPersonTel: 18848960162,
      urgencyPersonRelationship: '姐妹',
    },
  ],
  employeeManagementTabArr: [
    {
      id: 'a',
      tab: '全部',
    },
    {
      id: 'b',
      tab: '待完善资料',
    },
    {
      id: 'c',
      tab: '待入职',
    },
    {
      id: 'd',
      tab: '在职',
    },
    {
      id: 'e',
      tab: '待离职',
    },
    {
      id: 'f',
      tab: '已失效',
    },
    {
      id: 'g',
      tab: '人员异动',
    },
  ],
  typeSelectArr: [
    {
      key: 'Corvidae',
      val: '待完善',
    },
    {
      key: 'Wait',
      val: '待签署',
    },
    {
      key: 'Process',
      val: '签署中',
    },
    {
      key: 'Success',
      val: '签署成功',
    },
    {
      key: 'Fail',
      val: '签署失败',
    },
    {
      key: 'Refuse',
      val: '拒签',
    },
    {
      key: 'Cancel',
      val: '取消签署',
    },
  ],
  leaveModalReasonData: [
    '合同/协议终止到期',
    '试用期不合格',
    '个人原因',
    '被用人单位开除',
    '除名和辞退',
    '退休',
    '死亡',
    '自离',
  ],
  attachType: [
    {
      key: 'YCZ',
      val: '一寸照',
    },
    {
      key: 'SFZZM',
      val: '身份证正面',
    },
    {
      key: 'SFZFM',
      val: '身份证反面',
    },
    {
      key: 'XLZS',
      val: '学历证书',
    },
    {
      key: 'XWZS',
      val: '学位证书',
    },
    {
      key: 'LZZM',
      val: '前公司离职证明',
    },
  ],
}

export default datas
