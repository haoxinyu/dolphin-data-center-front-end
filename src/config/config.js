/*
 * @Author: your name
 * @Date: 2021-04-07 15:47:47
 * @LastEditTime: 2021-04-27 19:27:24
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \dolphin-data-center-front-end\src\config\config.js
 */
// 自定义配置，参考 ./default/setting.config.js，需要自定义的属性在这里配置即可
module.exports = {
  theme: {
    color: '#1890ff',
    mode: 'light',
    success: '#52c41a',
    warning: '#faad14',
    error: '#f5222f',
  },
  layout: 'head',
  multiPage: true,
  asyncRoutes: true, //异步加载路由，true:开启，false:不开启
  animate: {
    disabled: false,
    name: 'fade',
    direction: 'left',
  },
}
